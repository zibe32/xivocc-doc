XiVO-doc
========

This is the project of documentation of the software XiVO-CC.

Dependencies
------------

* Sphinx (package python-sphinx on Debian)

Build
-----

   make clean html

