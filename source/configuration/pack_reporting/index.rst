.. _pack_reporting:

************************
Reporting and statistics
************************

Introduction
============

Pack reporting is a part of the XiVO-CC. It aims at computing historical statistics, which are
stored in the **xivo_stats** database. Sample reports based on them are accessible in **SpagoBI**.


.. _attached_data:

Architecture
============

Several components are used to create and update all statistics tables.

* xivo_replic : Replicate tables from xivo to xivo_stats database and populate elasticsearch
* xivo_stats : Calculate call data in real time
* pack_reporting : Calculate agregated statistics every 15 mns and purge database

.. figure:: xivoccreporting.png
   :scale: 80%
   :alt: XiVOcc reporting architecture

Attached Data
=============

The pack reporting allows to attach as mush data as wished to a given call, in order to find them in the reporting database for future use.
This data must be in the form of a set of key-value pairs.

To attach data to a call, you must use the dialplan's **CELGenUserEvent** application:

.. code-block:: bash

   exten = s,n,CELGenUserEvent(ATTACHED_DATA,my_key=my_value)

This will insert the following tuple in the **attached_data** table:

+--------+----------+
| key    | value    |
+========+==========+
| my_key | my_value |
+--------+----------+

Database Description
====================

You can find the databese descrition :ref:`here <xivo-stats-data-base-description>`
