************
Totem Panels
************

Disable Totem Panels
====================

If you do not use pannels and elasticsearch, it is possible to disable then.

You need to edit the docker-xivocc.yml file.

**First**, in `xivo_replic` section:

* remove or comment the elasticsearch link,
* add elasticsearch as en `extra_host` on 127.0.0.1
* and add the `DISABLEELASTIC=true` environment variable

.. code-block:: ini

  xivo_replic:
    image: xivoxc/xivo-db-replication:latest
  
    links:
    - pgxivocc:db
    #- elasticsearch:elasticsearch
  
    extra_hosts:
    - "elasticsearch:127.0.0.1"
  
    environment:
    - JAVA_OPTS=-Xms256m -Xmx256m
    - XIVO_HOST=192.168.250.11
    - DISABLEELASTIC=true
  ...

**Secondly**, remove the `elasticsearch` and `kibana_volumes` sections,

**Thirdly**, in `nginx` section:

* remove or comment the elasticsearch link,
* add elasticsearch as en `extra_host` on 127.0.0.1
* remove or comment the kibana_volumes

.. code-block:: ini

  nginx:
    image: xivoxc/xivoxc_nginx:latest
  
    ports:
    - 80:80
  
    links:
    #- elasticsearch
    - xuc
    - xucmgt
  
    extra_hosts:
    - "xivo_host:192.168.250.11"
    - "elasticsearch:127.0.0.1"
  
    volumes_from:
    #- kibana_volumes
    - fingerboard
  ...

