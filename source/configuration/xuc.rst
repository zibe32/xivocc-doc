***
XuC
***

Ldap Authentication
===================

Configure LDAP authent for CCmanager, Web Assistant and Web Agent

You need to include in the compose.yml file a link to a specific configuration file by adding in xuc section a specific volume
and an environment variable to specify the alternate config file location

::

   xuc:

   ....

   environment:
   ....
   - CONFIG_FILE=/conf/xuc.conf

   volumes:
   - /etc/docker/xuc:/conf


Edit in /etc/docker/xuc/ a configuration file named xuc.conf to add ldap configuration (empty by default)


::

   include "application.conf"

   authentication {
     ldap {
       managerDN = "uid=company,ou=people,dc=company,dc=com"      # user with read rights on the whole LDAP
       managerPassword = "xxxxxxxxxx"                             # password for this user
       url = "ldap://ldap.company.com:389"                        # ldap URI
       searchBase = "ou=people,dc=company,dc=com"                 # ldap entry to use as search base
       userSearchFilter = "uid=%s"                                # filter to use to search users by login, using a string pattern
     }
   }

Recreate the container : `dcomp up -d xuc`


Pushing Phone Statuses To External Server
=========================================

The XuC is able to push phone status to external server (Bluemind, Zimbra ....)

* Add **API_eventUrl** environment variable to xuc section

.. code-block:: yaml

        xuc:

            image: xivoxc/xuc:latestast11

            ports:
            - 8090:9000

            environment:
            - JAVA_OPTS=-Xms512m -Xmx1024m
            - XIVO_HOST=192.168.XX.XXX
            - CONFIG_MGT_HOST=config
            - CONFIG_MGT_PORT=9000
            - API_eventUrl=http://192.168.XX.XXX:8080/service/extension/xivo/dropbox/



