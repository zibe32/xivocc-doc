########################################
XuC XiVO Unified Communication Framework
########################################

Xuc is an application server developed by Avencall_ Group
XuC is build on Play_ using intensively Akka_ and written in Scala_

.. _Avencall: http://www.avencall.com/
.. _XiVO: http://documentation.xivo.io/
.. _Play: http://www.playframework.com/
.. _Akka: http://akka.io/
.. _Scala: http://www.scala-lang.org/



Xuc server provides

* Javascript API
* Web services
* Sample application
* Contact center statistics

XuC is composed of 3 modules

* The server module
* The core module
* The statistic module.

.. toctree::
   :maxdepth: 2

   api/javascriptapi
   api/rest
   stats/statistics
   technical/structure

