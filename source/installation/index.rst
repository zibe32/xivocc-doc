************
Installation
************

The XiVO-CC software suite is made of several independent components.

Depending on your system size, they can be installed on separate virtual or physical machines.

In this section, we will explain how to install these components on a single machine.

.. toctree::
   :maxdepth: 3

   installation

