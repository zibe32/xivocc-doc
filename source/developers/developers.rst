.. highlightlang:: rest

.. _developper:

#########
Developer
#########

======================
Building and packaging
======================

Refer to each subproject readme file


========================
Documentation Guidelines
========================

The XiVOcc documentation_ uses `reStructuredText`_ as its markup language and is
built using `Sphinx`_. The source code is available on Gitlab : https://gitlab.com/xivoxc/xivocc-doc

.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _sphinx: http://sphinx.pocoo.org
.. _documentation: https://gitlab.com/xivoxc/xivocc-doc

XiVO-CC documentation is generated with Sphinx.

======
Sphinx
======

For more details see `The Sphinx Documentation <http://sphinx.pocoo.org/contents.html>`_

================
reStructuredText
================

For more details see `The reST Quickref <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_

Quick Reference

* http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt
* http://docutils.sourceforge.net/docs/user/rst/quickref.html
* http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html

Sections
--------

Section headings are very flexible in reST. We use the following convention in
the Xuc documentation:

* ``#`` (over and under) for module headings
* ``=`` for sections
* ``-`` for subsections
* ``^`` for subsubsections
* ``~`` for subsubsubsections


Cross-referencing
-----------------

Sections that may be cross-referenced across the documentation should be marked
with a reference. To mark a section use ``.. _ref-name:`` before the section
heading. The section can then be linked with ``:ref:`ref-name```. These are
unique references across the entire documentation.

Language
--------

The documentation must be written in english, and only in english.


Sections
--------

The top section of each file must be capitalized using the following rule:
capitalization of all words, except for articles, prepositions, conjunctions, and forms of to be.

Correct::

   The Vitamins are in My Fresh California Raisins

Incorrect::

   The Vitamins Are In My Fresh California Raisins

Use the following punctuation characters:

* ``*`` with overline, for "file title"
* ``=``, for sections
* ``-``, for subsections
* ``+``, for subsubsections
* ``^``, for subsubsubsections

Punctuation characters should be exactly as long as the section text.

Correct::

   Section1
   ========

Incorrect::

   Section2
   ==========

There should be 2 empty lines between sections, except when an empty section is followed by another
section.

Correct::

   Section1
   ========

   Foo.


   Section2
   ========

   Bar.

Correct::

   Section1
   ========

   Foo.


   .. _target:

   Section2
   ========

   Bar.

Correct::

   Section1
   ========

   Subsection1
   -----------

   Foo.

Incorrect::

   Section1
   ========

   Foo.

   Section2
   ========

   Bar.

For example::

  .. _xuc-module:

  #############
   Xuc Module
  #############

  This is the module documentation.

  .. _xuc-section:

  Xuc Section
  ============

  Xuc Subsection
  ---------------

  Here is a reference to "xuc section": :ref:`xuc-section` which will have the
  name "Xuc Section".

Lists
-----

Bullet lists::

   * First item
   * Second item

Autonumbered lists::

   #. First item
   #. Second item


Literal blocks
--------------

Use ``::`` on the same line as the line containing text when possible.

The literal blocks must be indented with three spaces.

Correct::

   Bla bla bla::

      apt-get update

Incorrect::

   Bla bla bla:

   ::

      apt-get update


Inline markup
-------------

Use the following roles when applicable:

* ``:file:`` for file, i.e.::

   The :file:`/dev/null` file.

* ``:menuselection:`` for the web interface menu::

   The :menuselection:`Configuration --> Management --> Certificates` page.

* ``:guilabel:`` for designating a specific GUI element::

   The :guilabel:`Action` column.


Others
------

* There must be no warning nor error messages when building the documentation with ``make html``.
* There should be one and only one newline character at the end of each file
* There should be no trailing whitespace at the end of lines
* Paragraphs must be wrapped and lines should be at most 100 characters long

Build the documentation
=======================

First install `Sphinx`_. See below.

Building
--------
For the html and pdf version of the docs::

    make html

Installing Sphinx and other tools
---------------------------------

To be able to generate pdf and documentation you need install Sphinx and other tools::

   sudo easy_install -U Sphinx
   sudo apt-get install texlive-latex-base texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended
    
